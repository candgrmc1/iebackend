<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SuggestionsIndex;
use Elasticsearch;
use App\Article;

class EsIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'es:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index Elastic Search Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = Elasticsearch\ClientBuilder::create()->build();	
        $searchables = ['id','title','category','abstract','body','created_at'];
        $articles = Article::select($searchables )->get();
        $hasIndice = $client->indices()->exists(['index' => 'articles']);

        if($hasIndice){
            $client->indices()->delete(['index' => 'articles']);
        }
        
        foreach($articles as $article){

            $params = array();
            
            $params['body']  = array(
                "settings" => [
                    'number_of_shards' => 2,
                    'number_of_replicas' => 0,
                    "index"=> [
                        "analysis"=> [
                            "filter"=> [
                                "english_stemmer"=> [
                                    "type"=> "stemmer",
                                    "name"=> "english"
                                ],
                            ],
                            "analyzer"=> [
                                "articles"=> [
                                    "tokenizer"=> "standard",
                                    "filter"=> [
                                        "english_stemmer",
                                        "lowercase",
                                        "trim"
                                        ]
                                    ]
                                ]
                            ]
                    ]
                ],
                "mappings" => [
                    "properties" => [
                        "title" =>  [ "type" => "string"],  
                        "abstract" =>  [ "type" => "string"  ], 
                        "body" =>   [ "type" => "string"  ],
                        "created_at" =>   [ "type" => "date"  ],
                        "category" =>   [ "type" => "string" ]     
                      ]
                    ],
                'id' => $article->id, 											
                'title' => $article->title,
                'abstract' => $article->abstract,
                'body' => $article->body,
                'category' => $article->category,
                'created_at' => $article->created_at
            );
            $params['index'] = 'articles';
            $params['type']  = 'article';
            $result = $client->index($params);

        }

       
        return 0;
    }
}
