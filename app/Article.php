<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
   
    /**
     * @var string
     */
    protected $fillable = ["author_id","category","title","abstract","body"];
    protected $table = "articles";

}