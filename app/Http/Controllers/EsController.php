<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Article;
use Elasticsearch;
use Response;
class EsController extends Controller
{
    //
    private $client;

    public function __construct(){
        $this->client = Elasticsearch\ClientBuilder::create()->build();
    }
    
    private function transform($result){
        $res = [];
        foreach($result['hits']['hits'] as $item){
            $source = $item['_source'];

            array_push($res,[
                'id' => $source['id'],
                'title' => $source['title'],
                'abstract' => $source['abstract'],
                'body' => $source['body'],
                'created_at' => $source['created_at'],
            ]);
        }
        return ['data' => $res];
    }

    public function search($q){
        $params['index'] = 'articles';						
        $params['type'] = 'article';
        $params['body']['query']['multi_match'] = [
            
            'query' => $q,
            'type' => 'best_fields',
            'fields' => [
                'title',
                'body',
                'abstract',
                'category^2',
            ],
            "fuzziness" => "1"
        ];
        $params['body']['sort'] = ["created_at" => [ "order" => "desc" ]];		
        $params['body']['track_scores'] = true;				

        $result = $this->client->search($params);
        dd($result);	
        return $this->transform($result);
}

}
