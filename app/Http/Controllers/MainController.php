<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Content;
class MainController extends Controller
{
    //
    public function index(Request $request, $q){
        $es = new EsController;
        $articles = $es->search($q ? $q : '');

        return response()->json($articles);
    }
}
