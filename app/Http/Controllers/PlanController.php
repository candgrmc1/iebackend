<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Plan;
class PlanController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request){
        $plans = Plan::where('is_active',1)->limit(10)->get();
        return response()->json($plans);
    }
}
