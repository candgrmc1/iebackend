<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|params
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
   
});

Route::middleware('auth:api')->get('/plans','PlanController@index');

Route::post('login', 'AuthController@login');
Route::get('/search/{q}', 'MainController@index');
