```bash
php -v
PHP 7.2.19
composer --version
Composer version 1.8.6 2019-06-11 15:03:05

docker pull docker.elastic.co/elasticsearch/elasticsearch:7.12.1

docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.12.1

composer update

php artisan migrate --seed

php artisan es:index

```